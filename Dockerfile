FROM tomcat

WORKDIR /usr/local/tomcat

COPY webapp/target/webapp.war /usr/local/tomcat/webapps/webapp.war
